# Atera ORM
> Like ORM, but awful!

This is mostly my attempt at centralizing and organizing a custom PHP database ORM system I've been
copypasting between projects.  It mostly acts as a bridge between different RDBMS' syntaxes, and provides a simpler
way of getting data to and from those databases.

Yes, it's terrible. I know.  

Atera was originally coded long ago, back when ADODB was in vogue, for my ChanMan \*chan indexing project. That
project snowballed into a Baby's Big Data project, and I hacked on Atera over the years to try and squeeze more
speed out of it.  Then I decided to recode 7chan and started using this shit for it, too. 7chan runs on Postgres,
so I slapped on a translation layer.  Then ADODB effectively died and both systems moved to PDO under the hood.
Now an SS13 server I screw with uses this as part of its CRM package.

So yeah, it's **A** **TER**rible **A**ttempt at **ORM**. Ha.  Punny names.

## Installing
* Add the following to your composer.json:
```json
{
  "repositories": [{
    "type": "vcs",
    "url": "https://gitlab.com/N3X15/atera-orm.git"
  }],
}
```
* Then run: `composer require n3x15/atera-orm`
* Your configuration will need to specify:
  * `DB_DRIVER` - Either `mysqli` or `postgres#` (6-9)
  * `DB_HOST`
  * `DB_PORT`
  * `DB_USERNAME`
  * `DB_PASSWORD`
  * `DB_PERSISTENT` - Leave `false` on postgres, causes problems

## Example Usecase

```php
use \Atera\DB;
require_once 'vendor/autoload.php';
// Optional: Set up error handling
DB::SetErrorHandler(function($message){
  die($message);
});
// Optional: Set up warning handling
DB::SetWarningHandler(function($message){
  echo $message;
});
// Connect to the server
DB::Initialize();

class Admin extends \Atera\DBTable
{
    public $ID = 0;
    public $CKey = '';
    public $Rank = '';
    public $Level = 0;
    public $Flags = 0;

    // NOTE: DO NOT USE  __construct!
    protected function onInitialize()
    {
        // The table this corresponds to
        $this->setTableName('erro_admin');

        // Associate the id column with $this->ID, and mark it as the primary key.
        // You can set multiple rows as the PK, but you must have at least one.
        $this->setFieldAssoc('id', 'ID', true);

        $this->setFieldAssoc('ckey', 'CKey');
        $this->setFieldAssoc('rank', 'Rank');
        $this->setFieldAssoc('level', 'Level');
        $this->setFieldAssoc('flags', 'Flags');

        // Convert the string from the level column to an int using intval() when reading a row.
        // Do not do anything to it when INSERTing, UPDATEing, etc.
        $this->setFieldTranslator('level', 'intval', null);
        $this->setFieldTranslator('flags', 'intval', null);
    }

    // Otherwise, DBTables work the same as any other class:
    public static function FindCKey($ckey)
    {
        $res = DB::GetRow('SELECT * FROM erro_admin WHERE ckey=?', [$ckey]);
        if ($res == null)
            return null;
        return self::FromRow($res);
    }
}

// Find N3X15 using the static method we made and return the record as an Admin instance.
$admin = Admin::FindCKey('N3X15');
// Set his rank to Host
$admin->Rank = 'Host';
// Send and COMMIT an UPDATE.
DB::Update($admin);
```
