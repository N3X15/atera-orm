<?php
namespace Atera\Translators;

class ArrayTranslator extends \Atera\Translators\Translator
{
    public $delimiter = ';';
    public function __construct($delim)
    {
        $this->delimiter = $delim;
    }

    public function toDB($input)
    {
        return implode($this->delimiter, $input);
    }

    public function fromDB($input)
    {
        return explode($this->delimiter, $input);
    }
}
