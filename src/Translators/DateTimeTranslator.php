<?php
namespace Atera\Translators;

class DateTimeTranslator extends \Atera\Translators\Translator
{

    public function toDB($input)
    {
        return $input;
    }

    public function fromDB($input)
    {
        return intval($input);
    }

    public function wrapSetSQL($input)
    {
        return "TIMESTAMP($input)";
    }

    public function wrapGetSQL($input)
    {
        return "UNIX_TIMESTAMP($input)";
    }
}
