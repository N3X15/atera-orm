<?php
namespace Atera\Translators;

class IPTranslator extends \Atera\Translators\Translator
{

    public function toDB($input)
    {
        return $input;
    }

    public function fromDB($input)
    {
        return $input;
    }

    public function wrapSetSQL($input)
    {
        return "INET6_ATON($input)";
    }

    public function wrapGetSQL($input)
    {
        return "INET6_NTOA($input)";
    }
}
