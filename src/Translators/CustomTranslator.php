<?php
namespace Atera\Translators;

class CustomTranslator extends \Atera\Translators\Translator
{
    private $to;
    private $from;

    public function __construct($to, $from)
    {
        $this->to = $to;
        $this->from = $from;
    }

    public function toDB($input)
    {
        return $to($input);
    }

    public function fromDB($input)
    {
        return $from($input);
    }
}
