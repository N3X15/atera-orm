<?php
namespace Atera\Translators;

class JSONTranslator extends \Atera\Translators\Translator
{

    public function toDB($input)
    {
        return json_encode($input);
    }

    public function fromDB($input)
    {
        return json_decode($input);
    }
}
