<?php
namespace Atera\Compatibility;
/**
 * Compatibility layer for MySQL/MariaDB.
 */
class MySQLCompatibility extends \Atera\Compatibility\DBCompatibility
{
    public $HighPriority = 'HIGH_PRIORITY';
    public $IdentEscapeChar = '`'; // Works with ANSI
}
